var fs = require('fs');
var child_process = require('child_process');
var root_path = 'data'
var crypto = require('crypto');


function getAllFiles(root){
	var res = [] , files = fs.readdirSync(root);
	files.forEach(function(file){
		var pathname = root+'/'+file, stat = fs.lstatSync(pathname);

		if (!stat.isDirectory()){
			res.push('./' + pathname);
		}
	});
	return res
}

var filelist = getAllFiles(root_path)

filelist.forEach(function(file) {
	var data = require(file)
	var title = data.title
	var tag = data.tag
	var filename = 't1/source/_posts/' +crypto.createHash('md5').update(title).digest("hex") + '.md';
	var md = '---\n'

	title = child_process.execSync('trans -b -e bing -s zh -t en', {input:title})
	md +='title:  ' + title + '\n'
	md += 'date: ' + new Date().toISOString().slice(0,10) + '\n'
	tag = child_process.execSync('trans -b -e bing -s zh -t en', {input:tag})
	md += 'tags:\n  - ' + data.tag + '\n'
	md += '---\n'
	md += '\n'

	var content = data.content
	content.forEach(function(line) {
		var text = line.split('--').pop().trim().replace(/。/g, '.')
		if (text.length != 0) {
			var en = child_process.execSync('trans -b -e bing -s zh -t en', {input:text})
			md += '\n' + en.toString() + '\n'
		}
	})
	fs.writeFileSync(filename,md)
	console.log(filename + ' done')
})