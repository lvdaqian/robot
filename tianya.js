"use strict";

let request = require('request');
let cheerio = require('cheerio');
let fs = require('fs');
let child = require('child_process');
let crypto = require('crypto');

var arts = [];


function loadHotArticale () {

	request('http://bbs.tianya.cn/hotArticle.jsp', function (error, response, body) {
	  if (!error && response.statusCode == 200) {
		let $ = cheerio.load(body)
		let list = $('td.td-title > a').each(function(i, el) {
			let textA = $(el).text().split(']')
			let title = textA.pop()
			let tag = textA[0].split('[').pop()
			let link = $(el).attr('href')
			arts.push({'title':title,'link':link, 'tag':tag})
		})

		fetchArticale(0)

	  }
	})

}

function fetchArticale (index) {
	if (arts.length <= index) {
		return
	}
	let address = arts[index].link
	let title = arts[index].title
	let tag = arts[index].tag
	let url = "http://bbs.tianya.cn/" + address

	request(url, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			let $ = cheerio.load(body)
			let host_id = $('.host-item').attr('_hostid')
			let host_content = $('.atl-item').filter(function (i, el) { return $(el).attr('_hostid') === host_id})
			var result = []
			host_content.each(function(i, el) {
				let content = $(el).children('.atl-content').children('.atl-con-bd').children('.bbs-content').text().split('\n')
				result = result.concat(content)
			})
				
			var data = {}
			data['title'] = title
			data['link'] = address
			data['content'] = result
			data['tag'] = tag

			let str = JSON.stringify(data)
			let filename = 'data/' + crypto.createHash('md5').update(title).digest("hex") + '.json';

			fs.writeFileSync(filename,str)
			console.log('finish: ' + url + result.length)
			fetchArticale(index+1)
		}
		
	})
}

loadHotArticale()